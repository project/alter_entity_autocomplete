# Alter Entity Autocomplete

The module provide you alter autocomplete field suggestions list according other
fields of entity.

- For a full description of the module, visit the
[project page](https://www.drupal.org/project/alter_entity_autocomplete)

- Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/alter_entity_autocomplete)


## Contents of this file

- Requirements
- Installation
- Configuration
- Usage
- Note
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

- Install the Alter Entity Autocomplete module as you would normally install a
contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- The configuration page is at admin/config/alter_entity_autocomplete,
where you can configure entity name i.e("user") and field name whhich you
want to use for autocomplete sugggestion i.e("mail").


## Usage

- Add Entity reference Field in your entity.
- Under configuration page enter user entity field name whichh you want to
alter.


## Note

- Currently Alter Entity Autocomplete supports only user entity.


## Maintainers

- Swati Chouhan - [swatichouhan012](https://www.drupal.org/u/swatichouhan012)
- dhiren mishra - [dhirendra.mishra](https://www.drupal.org/u/dhirendramishra)

Supporting organizations:

- Valuebound - [Valuebound](https://www.drupal.org/valuebound)
