<?php

namespace Drupal\alter_entity_autocomplete\AlterEntityAutocomplete;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\Core\Database\Database;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Create a Alter Entity Autocomplete Matcher service.
 */
class AlterEntityAutocompleteMatcher extends EntityAutocompleteMatcher {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager, QueryInterface $entityQuery) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityQuery = $entityQuery;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity.query.config')
    );
  }

  /**
   * Gets matched labels based on a given search string.
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {

    $matches = [];
    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];
    $handler = $this->selectionManager->getInstance($options);

    if (isset($string)) {
      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $match_limit);

      $target_entity_field = $this->configFactory->get('alter_autocomplete_field_name');

      if ($target_type == 'user' && !empty($target_entity_field)) {

        // Check selected roles.
        foreach ($selection_settings["filter"]["role"] as $key => $role) {
          if (!empty($role)) {
            $selected_roles[] = $role;
          }
        }
        $query = $this->entityQuery->get('user');
        $query->condition('status', TRUE);
        if (!empty($selected_roles)) {
          $query->condition('roles', $selected_roles, 'IN');
        }
        $query->condition($target_entity_field, '%' . Database::getConnection()->escapeLike($string) . '%', 'LIKE');
        $query->range(0, $match_limit);
        $result = $query->execute();

        if (empty($result)) {
          return [];
        }

        $entity_labels = [];
        $entities = $this->entityTypeManager->getStorage($target_type)->loadMultiple($result);
        foreach ($entities as $entity_id => $entity) {
          $bundle = $entity->bundle();
          if (!empty($target_entity_field)) {
            $entity_labels[$bundle][$entity_id] = !empty($entity->$target_entity_field->value) ? $entity->$target_entity_field->value : $entity->label();
          }
        }
      }
      // Loop through the entities and convert them into autocomplete output.
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {
          $key = "$label ($entity_id)";
          // Strip things like starting/trailing white spaces, line breaks and
          // tags.
          $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(strip_tags($key))));
          // Names containing commas or quotes must be wrapped in quotes.
          $key = Tags::encode($key);

          $matches[] = ['value' => $key, 'label' => $label];

        }
      }
    }
    return $matches;
  }

}
